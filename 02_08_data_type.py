a, b = ('python', 'life')
print(a)
print(b)

(a, b) = 'python', 'life'
print(a)
print(b)

a=b='python'
print(a)
print(b)

[a, b] = ['python', 'life']
print(a)
print(b)

a, b = b, a
print(a)
print(b)


a=[1,2,3]
b= a
a[1] = 4

print(a)
print(b)


# 리스트 복사
print()
a=[1,2,3]
b=a[:]
a[1] = 4
print(a)
print(b)
