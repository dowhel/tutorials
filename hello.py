print("Hello IDLE")

a = 0o177
b = 0xABC

print(a)
print(b)

a=3
b=4
print(a ** b)

print( 7 % 3)

print( 3 % 7)


print(7 / 4)
print(7 // 4)

print(-7 / 4)
print(-7 // 4)

multiline="""
Life is too short
You need python
"""
print(multiline)

a = "Life is too short, You need Python"
print(a[0])
print(a[12])
print(a[-1])

b=a[0] + a[1] + a[2] + a[3]
print(b)

print(a[0:3])

print(a[19:])

# a[19:-7]이 뜻하는 것은 a[19]에서부터 a[-8]까지를 말한다. 이 역시 a[-7]은 포함하지 않는다.
print(a[19:-7])

# 날짜 슬라이싱
print()
a="20010331Rainy"
year = a[:4]
day = a[4:8]
weather = a[8:]

print(year)
print(day)
print(weather)

print()
number = 3
print("I eat %d apples." % number)

number=10
day="three"
print("I ate %d apples. so I was sick for %s days." % (number, day))


print()
print("I have %s apples" % 3)
print("rate is %s" % 3.234)

print("Error is %d %%." % 98)

print()
print("%10s" % "hi")
print("%-10sjane." % 'hi')
print("%0.4f" % 3.42134234)
print("%10.4f" % 3.42134234)

print()
a="hobby"
print(a)
print(a.count('b'))

print()
a = 'Pyhthon is best choice'
print(a)
print(a.find('b'))
print(a.find('k'))


print()
a="Life is too short"
print(a)
print(a.index('t'))
# print(a.index('k'))

a=','
print(a.join('abcd'))
