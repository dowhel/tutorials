# 딕셔너리 타입
a={ 1: 'a'}
print(a)
a[2] = 'b'
print(a)


a['name'] = 'pey'
print(a)
a[3] = [1,2,3]
print(a)


del a[1]
print(a)

print()

# 딕셔너리 활용
grade = {'pey': 10, 'julliet': 99}
print(grade['pey'])
print(grade['julliet'])

print(grade.keys())
print(grade.values())
print(grade.items())

a={'name':'pey', 'phone':'0119993323', 'brith': '1118'}
print('name' in a)
print('email' in a)