a=[]
b=[1, 2, 3]
c=['Life', 'is', 'too', 'short']
d=[1, 2, 'Life', 'is']
e=[1, 2, ['Life', 'is']]

print()
print(a)
print(b)
print(c)
print(d)
print(e)

print()
f = list()
print(f)

# 리스트 더하기
print()
a = [1, 2, 3]
b = [4, 5, 6]
print(a + b)

a = [1, 2, 3]
print(a * 3)

# 형변환
print()
a = [1, 2, 3]
print(str(a[2]) + " hi")

# 리스트 수정
print()
a = [1,2,3]
print(a)
a[2] = 4
print(a)

a[1] = ['a', 'b', 'c']
print(a)

a=[1, 2, 4]
print(a[1:2])
a[1:2] = ['a', 'b', 'c']
print(a)

a[1:3] = []
print(a)

del a[1]
print(a)

# append
print()
a=[1,2,3]
print(a)
a.append(4)
print(a)

a.append([5, 6])
print(a)

# 정렬
print()
a=[1,4,3,2]
print(a)
a.sort()
print(a)

a=['a', 'c', 'b']
print(a)
a.sort()
print(a)

# 뒤집기
print()
a=['a', 'c', 'b']
print(a)
a.reverse()
print(a)

b='test string'
print(b)
