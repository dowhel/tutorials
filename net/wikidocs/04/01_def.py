def sum(a,b):
    result = a + b
    return result

a = 3
b = 4
c = sum(a, b)
print(c)

d = sum(4,5)
print(d)

# 입력값없는 함수
print()
def say():
    return 'Hi'

a = say()
print(a)

