def sum(a, b):
    print("%d, %d의 합은 %d입니다." % (a, b, a+b))

print(sum(3, 4))


# 여러개의 입력값을 받는 함수
def sum_many(*args):
    sum = 0
    for i in args:
        sum = sum + i
    return sum

result = sum_many(1,2,3,4)
print(result)
