class Bird:
    def fly(self):
        # raise NotImplementedError
        print('very fast')

class Eagle(Bird):
    pass

eagel = Eagle()
eagel.fly()


print()


class MyError(Exception):
    def __str__(self):
        return '호용되지 않는 별명입니다.'

def say_nick(nick):
    if nick == '바보':
        raise MyError()
    print(nick)

try:
    say_nick('천사')
    say_nick('바보')
except MyError as e:
    print(e)

