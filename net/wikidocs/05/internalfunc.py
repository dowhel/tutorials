for i, name in enumerate(['body', 'foo', 'bar']):
    print(i, name)


print()

print(eval('1+2'))
print(eval("'hi' + 'a'"))

print(list(filter(lambda x: x>0, [1, -3, 2, 0, -5, 6])))

print()
sum = lambda a, b, : a + b
print(sum(3,4))

print()
def two_times(x): return x*2
print(list(map(two_times, [1,2,3,4,])))

print(list(map(lambda a:a*2, [3,4,5,6])))


